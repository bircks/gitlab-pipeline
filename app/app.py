from flask import Flask
from markupsafe import escape


app = Flask(__name__)


@app.route('/<variable>')
def show_result(variable):
    return 'Variable is: %s' % escape(variable)


def function_one():
    return 'one'


if __name__ == "__main__":
    app.run()
