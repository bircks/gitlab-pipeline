For at sætte CI/CD op har jeg valgt at lave en simpel applikation. Applikationen har nogle funktioner der returnerer resultater og viser dem på en hjemmeside.

På den måde kan jeg teste om de enkelte funktioner fungerer efterhånden som jeg tilføjer dem (CI) og jeg kan se om applikationen er deployet rigtigt på den server den leveres til (CD).